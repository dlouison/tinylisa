﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TinyLISA.Business;

namespace TinyLISA.BackOffice.UserControls.Clients
{
    /// <summary>
    /// Logique d'interaction pour CreateUpdateClientUserControl.xaml
    /// </summary>
    public partial class CreateUpdateClientUserControl : BaseCreateUpdateUC<Client>
    {

        #region Properties
        /// <summary>
        /// Get or set the store for the client to create or update.
        /// </summary>
        public Store Store { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="client">The client to create or update.</param>
        public CreateUpdateClientUserControl(Client client, Store store)
        {
            ToCreateOrUpdate = client;
            Store = store;

            this.DataContext = this;

            InitializeComponent();
        }

        #endregion

        #region Methods


        /// <summary>
        /// Add the modifications to save in DB.
        /// </summary>
        /// <param name="bmContainer">The BusinessModelsContainer from EntityFramework.</param>
        public override void AddToSave(BusinessModelsContainer bmContainer)
        {
            // Check if it is un update or a create.
            if (Store != null && bmContainer.StoreSet.Any(x => x.Id == Store.Id))
            {
                if (ToCreateOrUpdate.Id != 0)
                {
                    // If the id is not 0, it is un update.
                    var toUpdate = bmContainer.ClientSet.First(e => e.Id == ToCreateOrUpdate.Id);

                    if (toUpdate != null)
                    {
                        // Update the property of the client with the new ones.
                        toUpdate.Firstname = ToCreateOrUpdate.Firstname;
                        toUpdate.Lastname = ToCreateOrUpdate.Lastname;
                        toUpdate.Email = ToCreateOrUpdate.Email;
                        toUpdate.Phone = ToCreateOrUpdate.Phone;
                        toUpdate.City = ToCreateOrUpdate.City;
                        toUpdate.Adress = ToCreateOrUpdate.Adress;
                        toUpdate.PostalCode = ToCreateOrUpdate.PostalCode;
                        toUpdate.Store = bmContainer.StoreSet.First(x => x.Id == Store.Id);
                    }
                }
                else
                {
                    ToCreateOrUpdate.Store = bmContainer.StoreSet.First(x => x.Id == Store.Id);
                    bmContainer.ClientSet.Add(ToCreateOrUpdate);
                }
            }

        }

        #endregion


    }
}

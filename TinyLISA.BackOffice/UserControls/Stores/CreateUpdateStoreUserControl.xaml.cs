﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TinyLISA.BackOffice.Helpers;
using TinyLISA.Business;

namespace TinyLISA.BackOffice.UserControls.Stores
{
    /// <summary>
    /// Logique d'interaction pour CreateUpdateStoreUserControl.xaml
    /// </summary>
    public partial class CreateUpdateStoreUserControl : BaseCreateUpdateUC<Store>
    {
        #region Constructors
        /// <summary>
        /// Main constructor.
        /// </summary>
        /// <param name="store">The store to create or update.</param>
        public CreateUpdateStoreUserControl(Store store)
        {
            ToCreateOrUpdate = store;

            this.DataContext = this;

            InitializeComponent();
        }
        #endregion

        #region Methods


        /// <summary>
        /// Add the modifications to save in DB.
        /// </summary>
        /// <param name="bmContainer">The BusinessModelsContainer from EntityFramework.</param>
        public override void AddToSave(BusinessModelsContainer bmContainer)
        {
            // Check if it is un update or a create.
            if (ToCreateOrUpdate.Id != 0)
            {
                // If the id is not 0, it is un update.
                var toUpdate = bmContainer.StoreSet.First(e => e.Id == ToCreateOrUpdate.Id);

                if (toUpdate != null)
                {
                    // Update the property of the store with the new ones.
                    toUpdate.Name = ToCreateOrUpdate.Name;
                    toUpdate.Description = ToCreateOrUpdate.Description;
                }
            }
            else
                bmContainer.StoreSet.Add(ToCreateOrUpdate);

        }

        #endregion


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TinyLISA.Business;

namespace TinyLISA.BackOffice.UserControls.Catalogs
{
    /// <summary>
    /// Logique d'interaction pour CreateUpdateCatalogUserControl.xaml
    /// </summary>
    public partial class CreateUpdateCatalogUserControl : BaseCreateUpdateUC<Catalog>
    {

        #region Properties
        public Store Store { get; set; }
        #endregion

        #region Constructors
        public CreateUpdateCatalogUserControl(Catalog toCreateUpdate, Store store)
        {
            this.Store = store;
            this.ToCreateOrUpdate = toCreateUpdate;
            toCreateUpdate.ReleaseDate = DateTime.Now;

            this.DataContext = this;

            InitializeComponent();
        }
        #endregion

        #region Methods
        public override void AddToSave(BusinessModelsContainer bmContainer)
        {
            if (this.Store != null)
            {
                // Check if it is un update or a create.
                if (ToCreateOrUpdate.Id != 0 && bmContainer.CatalogSet.Any(x => x.Id == ToCreateOrUpdate.Id))
                {
                    // If the id is not 0, it is un update.
                    var toUpdate = bmContainer.CatalogSet.First(e => e.Id == ToCreateOrUpdate.Id);

                    if (toUpdate != null)
                    {
                        // Update the property of the client with the new ones.
                        toUpdate.Name = ToCreateOrUpdate.Name;
                        toUpdate.Description = ToCreateOrUpdate.Description;
                        toUpdate.ReleaseDate = ToCreateOrUpdate.ReleaseDate;
                    }
                }
                else
                {
                    ToCreateOrUpdate.Store = bmContainer.StoreSet.First(x => x.Id == Store.Id);
                    bmContainer.CatalogSet.Add(ToCreateOrUpdate);
                }
            }

        }

        #endregion


    }
}

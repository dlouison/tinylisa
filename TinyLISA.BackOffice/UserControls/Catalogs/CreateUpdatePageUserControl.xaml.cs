﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TinyLISA.Business;
using Xceed.Wpf.Toolkit;

namespace TinyLISA.BackOffice.UserControls.Catalogs
{
    /// <summary>
    /// Logique d'interaction pour CreateUpdatePageUserControl.xaml
    /// </summary>
    public partial class CreateUpdatePageUserControl : BaseCreateUpdateUC<Business.Page>
    {

        #region Properties
        public Catalog Catalog { get; set; }
        #endregion

        #region Constructors
        public CreateUpdatePageUserControl(Business.Page toCreateUpdate, Catalog catalog)
        {
            this.ToCreateOrUpdate = toCreateUpdate;
            this.Catalog = catalog;


            InitializeComponent();

            this.setPageNumber();
        }

        #endregion

        #region Methods


        private void setPageNumber()
        {
            // If it is not an update
            if (this.ToCreateOrUpdate.Id == 0)
            {
                var nextNumber = 1;
                // We set the next number by default

                using (var bmContainer = new BusinessModelsContainer())
                {
                    var catalogPages = bmContainer.CatalogSet.First(e => e.Id == this.Catalog.Id).Page.OrderBy(e => e.Number);

                    if (catalogPages.Count() > 0)
                        nextNumber = catalogPages.Last().Number + 1;
                }

                this.ToCreateOrUpdate.Number = nextNumber;
            }
        }


        public override void AddToSave(BusinessModelsContainer bmContainer)
        {
            // Check if it is un update or a create.
            if (this.Catalog != null && bmContainer.CatalogSet.Any(x => x.Id == this.Catalog.Id))
            {
                if (ToCreateOrUpdate.Id != 0)
                {
                    // If the id is not 0, it is un update.
                    var toUpdate = bmContainer.PageSet.First(e => e.Id == ToCreateOrUpdate.Id);

                    if (toUpdate != null)
                    {
                        toUpdate.Number = ToCreateOrUpdate.Number;
                    }
                }
                else
                {
                    // Si aucune page avec ce numéro dans ce catalogue n'existe
                    if (!bmContainer.PageSet.Any(e => e.Number == ToCreateOrUpdate.Number && e.Catalog.Id == this.Catalog.Id))
                    {
                        ToCreateOrUpdate.Catalog = bmContainer.CatalogSet.First(x => x.Id == this.Catalog.Id);
                        bmContainer.PageSet.Add(ToCreateOrUpdate);
                    }

                }
            }

        }

        #endregion


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TinyLISA.Business;

namespace TinyLISA.BackOffice.UserControls.Catalogs
{
    /// <summary>
    /// Logique d'interaction pour CreateUpdateProductUserControl.xaml
    /// </summary>
    public partial class CreateUpdateProductUserControl : BaseCreateUpdateUC<Product>
    {

        #region Properties
        public Business.Page Page { get; set; }
        #endregion

        #region Constructors
        public CreateUpdateProductUserControl(Product toCreateUpdate, Business.Page page)
        {
            this.Page = page;
            this.ToCreateOrUpdate = toCreateUpdate;

            this.DataContext = this;
            InitializeComponent();
        }
        #endregion

        #region Methods

        public override void AddToSave(BusinessModelsContainer bmContainer)
        {
            if (this.Page != null)
            {
                // Check if it is un update or a create.
                if (ToCreateOrUpdate.Id != 0 && bmContainer.ProductSet.Any(x => x.Id == ToCreateOrUpdate.Id))
                {
                    // If the id is not 0, it is un update.
                    var toUpdate = bmContainer.ProductSet.First(e => e.Id == ToCreateOrUpdate.Id);

                    if (toUpdate != null)
                    {
                        // Update the property of the client with the new ones.
                        toUpdate.Label = ToCreateOrUpdate.Label;
                        toUpdate.Description = ToCreateOrUpdate.Description;
                        toUpdate.Price = ToCreateOrUpdate.Price;
                    }
                }
                else
                {
                    ToCreateOrUpdate.Page = bmContainer.PageSet.First(x => x.Id == this.Page.Id);

                    bmContainer.ProductSet.Add(ToCreateOrUpdate);
                }
            }

        }
        #endregion

    }
}

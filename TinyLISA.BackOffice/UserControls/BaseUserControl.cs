﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace TinyLISA.BackOffice.UserControls
{
    public class BaseUserControl : UserControl, INotifyPropertyChanged, INotifyPropertyChanging
    {

        #region Properties

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;

        #endregion

        #region Methods

        /// <summary>
        /// Notify that the parameter passed property is changing.
        /// </summary>
        /// <param name="action"></param>
        public void OnPropertyChanging(Expression<Func<object>> action)
        {
            PropertyChangingEventHandler handler = PropertyChanging;
            if (handler != null)
            {
                var expression = (MemberExpression)action.Body;
                handler(this, new PropertyChangingEventArgs(expression.Member.Name));
            }
        }

        /// <summary>
        /// Notify that the parameter passed property has changed.
        /// </summary>
        /// <param name="action"></param>
        public void OnPropertyChanged(Expression<Func<object>> action)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                if (action.Body is MemberExpression)
                {
                    var expression = (MemberExpression)action.Body;
                    handler(this, new PropertyChangedEventArgs(expression.Member.Name));
                }
            }
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TinyLISA.BackOffice.Helpers;
using TinyLISA.Business;
using TinyLISA.Business.Interfaces;

namespace TinyLISA.BackOffice.UserControls
{
    public class BaseCreateUpdateUC<T> : BaseUserControl
    {

        #region Properties

        /// <summary>
        /// Define the object to create or update.
        /// </summary>
        private T _toCreateOrUpdate;
        /// <summary>
        /// Get or set the object to create.
        /// </summary>
        public T ToCreateOrUpdate
        {
            get { return _toCreateOrUpdate; }
            set
            {
                _toCreateOrUpdate = value;
                OnPropertyChanged(() => ToCreateOrUpdate);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Check if the object is OK and save the changes into the DB.
        /// </summary>
        /// <returns>True if the object has been saved.</returns>
        public virtual bool Save()
        {
            using (var bmContainer = new BusinessModelsContainer())
            {
                var errors = Validate(bmContainer);
                // If there is no error.
                if (string.IsNullOrEmpty(errors))
                {
                    try
                    {
                        // Save changes into DB.
                        bmContainer.SaveChanges();
                        return true;
                    }
                    catch (DbEntityValidationException validationException)
                    {
                        AlertHelper.ShowAlert(validationException.Message, AlertHelper.AlertTypes.Error);
                    }
                }
                else
                {
                    AlertHelper.ShowAlert(errors, AlertHelper.AlertTypes.Error);
                }
            }
            return false;
        }

        /// <summary>
        /// Add to the bmContainer the objects to create or update.
        /// </summary>
        /// <param name="bmContainer">The BusinessModelsContainer from EntityFramework.</param>
        public virtual void AddToSave(BusinessModelsContainer bmContainer) { }
        
        /// <summary>
        /// Validate the ToCreateOrUpdate object.
        /// </summary>
        /// <param name="bmContainer">The BusinessModelsContainer from EntityFramework.</param>
        /// <returns></returns>
        public virtual string Validate(BusinessModelsContainer bmContainer)
        {
            StringBuilder errors = new StringBuilder();

            AddToSave(bmContainer);

            if (bmContainer.GetValidationErrors().Count() > 0)
            {
                foreach (var item in bmContainer.GetValidationErrors().ToArray()[0].ValidationErrors)
                    errors.AppendLine(item.ErrorMessage);

            }

            return errors.ToString();
        }

        /// <summary>
        /// Close the window when the user click on cancel button.
        /// </summary>
        internal void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).DialogResult = false;
            Window.GetWindow(this).Close();
        }
        /// <summary>
        /// Save the object when the user click on save button.
        /// </summary>
        internal void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            if (Save())
            {
                Window.GetWindow(this).DialogResult = true;
                Window.GetWindow(this).Close();
            }
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyLISA.BackOffice.Interfaces;
using TinyLISA.Business;

namespace TinyLISA.BackOffice.Controllers
{
    class StoreController : IController<Store>
    {
        public bool Delete(Store toDelete)
        {

            throw new NotImplementedException();
        }

        public Store Update(Store toUpdate)
        {
            throw new NotImplementedException();
        }

        public Store Create(Store toCreate)
        {
            using (var bModelContainer = new BusinessModelsContainer())
            {
                using (var trans = bModelContainer.Database.BeginTransaction())
                {
                    bModelContainer.StoreSet.Add(toCreate);

                    if (bModelContainer.GetValidationErrors().Count() == 0)
                        trans.Commit();
                    else
                        trans.Rollback();
                }
            }

            return null;
        }
        /// <summary>
        /// Return all stores from database into an observable collection.
        /// </summary>
        /// <returns>An observable collection that contains all stores.</returns>
        public ObservableCollection<Store> GetAll()
        {
            ObservableCollection <Store> stores = new ObservableCollection<Store>();

            using (var bModelContainer = new BusinessModelsContainer())
            {
                foreach (var item in bModelContainer.StoreSet.ToList())
                    stores.Add(item);
            }

            return stores;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyLISA.BackOffice.Interfaces;
using TinyLISA.Business.Interfaces;

namespace TinyLISA.BackOffice.Controllers
{
    /// <summary>
    /// Define a base controller to use CRUD functions on models.
    /// </summary>
    class BaseController
    {
        /// <summary>
        /// Save an object into DB.
        /// </summary>
        /// <param name="toCreate">Object to save in DB.</param>
        /// <returns>Saved object.</returns>
        public object Create(IModel toCreate)
        {
            using (var bModelContainer = new Business.BusinessModelsContainer())
            {

            }

            return toCreate;
        }

        public bool Delete(IModel toDelete)
        {
            throw new NotImplementedException();
        }

        public object Update(IModel toUpdate)
        {
            using (var bModelContainer = new Business.BusinessModelsContainer())
            {
                using(DbContextTransaction trans = bModelContainer.Database.BeginTransaction())
                {
                    trans.Commit();
                }
            }
            throw new NotImplementedException();
        }
    }
}

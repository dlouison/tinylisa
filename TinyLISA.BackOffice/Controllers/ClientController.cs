﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyLISA.BackOffice.Interfaces;
using TinyLISA.Business;

namespace TinyLISA.BackOffice.Controllers
{
    class ClientController : IController<Client>
    {
        public Client Create(Client toCreate)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Client toDelete)
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<Client> GetAll()
        {
            ObservableCollection<Client> clients = new ObservableCollection<Client>();

            using (var bModelContainer = new BusinessModelsContainer())
            {
                foreach (var item in bModelContainer.ClientSet.ToList())
                    clients.Add(item); 
            }

            return clients;
        }

        public ObservableCollection<Client> GetAllForStore(Store store)
        {
            ObservableCollection<Client> clients = new ObservableCollection<Client>();

            using (var bModelContainer = new BusinessModelsContainer())
            {
                if (bModelContainer.StoreSet.Find(store.Id) != null)
                {
                    foreach (var item in bModelContainer.StoreSet.Find(store.Id).Client.ToList())
                        clients.Add(item);
                }

            }

            return clients;
        }


        public Client Update(Client toUpdate)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TinyLISA.BackOffice.Helpers;
using TinyLISA.BackOffice.UserControls.Clients;
using TinyLISA.BackOffice.UserControls.Stores;
using TinyLISA.BackOffice.Views;
using TinyLISA.Business;

namespace TinyLISA.BackOffice.ViewModel
{
    public class StoreViewModel : ViewModelBase
    {

        #region Properties
        /// <summary>
        /// Define the list of stores.
        /// </summary>
        private List<Store> _storeList;
        /// <summary>
        /// Get or set the list of stores.
        /// </summary>
        public List<Store> StoreList
        {
            get { return _storeList; }
            set
            {
                _storeList = value;
                RaisePropertyChanged("StoreList");
            }
        }
        /// <summary>
        /// Define the selected store.
        /// </summary>
        private Store _selectedStore;
        /// <summary>
        /// Get or set the selected store.
        /// </summary>
        public Store SelectedStore
        {
            get { return _selectedStore; }
            set
            {
                _selectedStore = value;
                RaisePropertyChanged("SelectedStore");
                RefreshClients();

                if (this.CatalogViewModel != null)
                    this.CatalogViewModel.RefreshCatalogs();
            }
        }
        /// <summary>
        /// Define the clients list.
        /// </summary>
        private List<Client> _clientList;
        /// <summary>
        /// Get or set the clients list.
        /// </summary>
        public List<Client> ClientList
        {
            get { return _clientList; }
            set
            {
                _clientList = value;
                RaisePropertyChanged("ClientList");
            }
        }

        /// <summary>
        /// Define the selected client.
        /// </summary>
        private Client _selectedClient;

        /// <summary>
        /// Get or set the selected client.
        /// </summary>
        public Client SelectedClient
        {
            get { return _selectedClient; }
            set
            {
                _selectedClient = value;
                RaisePropertyChanged("SelectedClient");
            }
        }

        public CatalogViewModel CatalogViewModel { get; set; }

        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public StoreViewModel()
        {
            RefreshStores();
        }


        #endregion

        #region Methods

        #region Store CRUD

        public void AddStore(Window window)
        {
            DialogHelper.ShowCreateOrUpdateModal<CreateUpdateStoreUserControl>(
               new CreateUpdateStoreUserControl(new Store()),
               window,
               "Créer un nouveau magasin");

            RefreshStores();
        }

        public void UpdateStore(Window window)
        {
            if (SelectedStore != null)
            {
                DialogHelper.ShowCreateOrUpdateModal<CreateUpdateStoreUserControl>(
                    new CreateUpdateStoreUserControl(SelectedStore),
                    window,
                    "Modifier le magasin");

                RefreshStores();
            }
        }

        public void DeleteStore()
        {
            if (AlertHelper.ShowAlert("Voulez-vous vraiment supprimer ce magasin ? Ceci supprimera tous les clients associés.", AlertHelper.AlertTypes.Question) == MessageBoxResult.Yes)
            {

                if (SelectedStore != null)
                {
                    using (var bmContainer = new BusinessModelsContainer())
                    {
                        var selectedStore = bmContainer.StoreSet.First(elem => elem.Id == SelectedStore.Id);

                        if (selectedStore != null)
                        {
                            bmContainer.StoreSet.Remove(selectedStore);
                            bmContainer.SaveChanges();

                            AlertHelper.ShowAlert("Le magasin a été supprimé");

                            RefreshStores();
                        }
                    }
                }
            }
        }

        #endregion

        #region Client CRUD
        /// <summary>
        /// Shows a popup to add a new client for the selected store.
        /// </summary>
        /// <param name="window">The parent window.</param>
        public void AddClient(Window window)
        {
            if (SelectedStore != null)
            {
                var toCreate = new Client();

                if (DialogHelper.ShowCreateOrUpdateModal(new CreateUpdateClientUserControl(toCreate, SelectedStore), window, "Créer un nouveau client"))
                    RefreshClients();
            }
        }
        /// <summary>
        /// Shows a modal to update the selected client.
        /// </summary>
        /// <param name="window">The parent window.</param>
        public void UpdateClient(Window window)
        {
            if (SelectedClient != null)
            {
                if (DialogHelper.ShowCreateOrUpdateModal(new CreateUpdateClientUserControl(SelectedClient, SelectedStore), window, "Modifier un client"))
                    RefreshClients();
            }
        }
        /// <summary>
        /// Delete the selected client from DB.
        /// </summary>
        public void DeleteClient()
        {
            if (SelectedClient != null)
            {
                if (AlertHelper.ShowAlert("Voulez-vous vraiment supprimer ce client ?", AlertHelper.AlertTypes.Question) == MessageBoxResult.Yes)
                {
                    using (var bmContainer = new BusinessModelsContainer())
                    {
                        if (bmContainer.ClientSet.Any(x => x.Id == SelectedClient.Id))
                        {
                            bmContainer.ClientSet.Remove(bmContainer.ClientSet.First(x => x.Id == SelectedClient.Id));

                            bmContainer.SaveChanges();

                            AlertHelper.ShowAlert("Le client a été supprimé.");

                            RefreshClients();
                        }
                    }

                }
            }
        }

        #endregion

        #region Refresh methods

        public void RefreshStores()
        {
            using (var bModelContainer = new BusinessModelsContainer())
            {
                this.StoreList = bModelContainer.StoreSet.OrderBy(x => x.Name).ToList();
            }

        }

        public void RefreshClients()
        {
            if (SelectedStore == null)
            {
                ClientList = new List<Client>();
            }
            else
            {
                using (var bModelContainer = new BusinessModelsContainer())
                {
                    if (bModelContainer.StoreSet.Any(x => x.Id == SelectedStore.Id))
                    {
                        Store selectedStoreTemp = bModelContainer.StoreSet.First(x => x.Id == SelectedStore.Id);
                        ClientList = selectedStoreTemp.Client.OrderBy(x => x.Lastname).ThenBy(x => x.Firstname).ToList();
                    }
                }
            }
        }

        #endregion

        #endregion

    }
}

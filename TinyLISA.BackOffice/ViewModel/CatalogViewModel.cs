﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using TinyLISA.BackOffice.Helpers;
using TinyLISA.BackOffice.UserControls.Catalogs;
using TinyLISA.BackOffice.Views;
using TinyLISA.Business;

namespace TinyLISA.BackOffice.ViewModel
{
    public class CatalogViewModel : ViewModelBase
    {

        #region Properties
        public Store SelectedStore
        {
            get
            {
                if (this.StoreViewModel != null)
                    return this.StoreViewModel.SelectedStore;

                return null;
            }
        }
        public StoreViewModel StoreViewModel { get; set; }
        /// <summary>
        /// Define the list of catalogs.
        /// </summary>
        private List<Catalog> _catalogs;
        /// <summary>
        /// Get or set the list of catalogs.
        /// </summary>
        public List<Catalog> Catalogs
        {
            get { return _catalogs; }
            set
            {
                _catalogs = value;
                RaisePropertyChanged(() => Catalogs);
            }
        }
        /// <summary>
        /// Define the selected catalog.
        /// </summary>
        private Catalog _selectedCatalog;
        /// <summary>
        /// Get or set the selected catalog.
        /// </summary>
        public Catalog SelectedCatalog
        {
            get { return _selectedCatalog; }
            set
            {
                _selectedCatalog = value;
                RaisePropertyChanged(() => SelectedCatalog);
                RefreshPages();
            }
        }


        private List<Page> _pages;


        public List<Page> Pages
        {
            get { return _pages; }
            set
            {
                _pages = value;
                RaisePropertyChanged(() => Pages);
            }
        }


        private Page _selectedPage;

        public Page SelectedPage
        {
            get { return _selectedPage; }
            set
            {
                _selectedPage = value;
                RaisePropertyChanged(() => SelectedPage);
                RefreshProducts();
            }
        }
        private List<Product> _products;

        public List<Product> Products
        {
            get { return _products; }
            set
            {
                _products = value;
                RaisePropertyChanged(() => Products);
            }
        }

        private Product _selectedProduct;

        public Product SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                RaisePropertyChanged(() => SelectedProduct);
            }
        }


        #endregion

        #region Constructors

        public CatalogViewModel(StoreViewModel storeViewModel)
        {
            this.StoreViewModel = storeViewModel;
            RefreshCatalogs();
        }

        #endregion

        #region Methods

        public void RefreshCatalogs()
        {
            using (var bmContainer = new BusinessModelsContainer())
            {
                Store selectedStore = null;

                if (this.StoreViewModel != null)
                    selectedStore = this.StoreViewModel.SelectedStore;


                if (selectedStore != null)
                    Catalogs = bmContainer.CatalogSet.Where(x => x.Store.Id == selectedStore.Id).ToList();
                else
                    Catalogs = new List<Catalog>();
            }
        }

        private void RefreshProducts()
        {
            using (var bmContainer = new BusinessModelsContainer())
            {
                if (this.SelectedPage != null)
                    Products = bmContainer.PageSet.First(x => x.Id == SelectedPage.Id).Product.OrderBy(x => x.Price).ToList();
                else
                    Products = new List<Product>();
            }
        }

        private void RefreshPages()
        {
            using (var bmContainer = new BusinessModelsContainer())
            {
                if (this.SelectedCatalog != null)
                    Pages = bmContainer.CatalogSet.First(x => x.Id == SelectedCatalog.Id).Page.OrderBy(x => x.Number).ToList();
                else
                    Pages = new List<Page>();
            }
        }



        #endregion


        #region CRUD

        //public void AddStore(Window window)
        //{
        //    DialogHelper.ShowCreateOrUpdateModal<CreateUpdateStoreUserControl>(
        //       new CreateUpdateStoreUserControl(new Store()),
        //       window,
        //       "Créer un nouveau magasin");

        //    RefreshStores();
        //}

        //public void UpdateStore(Window window)
        //{
        //    if (SelectedStore != null)
        //    {
        //        DialogHelper.ShowCreateOrUpdateModal<CreateUpdateStoreUserControl>(
        //            new CreateUpdateStoreUserControl(SelectedStore),
        //            window,
        //            "Modifier le magasin");

        //        RefreshStores();
        //    }
        //}

        //public void DeleteStore()
        //{
        //    if (AlertHelper.ShowAlert("Voulez-vous vraiment supprimer ce magasin ? Ceci supprimera tous les clients associés.", AlertHelper.AlertTypes.Question) == MessageBoxResult.Yes)
        //    {

        //        if (SelectedStore != null)
        //        {
        //            using (var bmContainer = new BusinessModelsContainer())
        //            {
        //                var selectedStore = bmContainer.StoreSet.First(elem => elem.Id == SelectedStore.Id);

        //                if (selectedStore != null)
        //                {
        //                    bmContainer.StoreSet.Remove(selectedStore);
        //                    bmContainer.SaveChanges();

        //                    AlertHelper.ShowAlert("Le magasin a été supprimé");

        //                    RefreshStores();
        //                }
        //            }
        //        }
        //    }
        //}

        internal void AddCatalog(Window window)
        {
            if (this.SelectedStore != null)
            {
                var res = DialogHelper.ShowCreateOrUpdateModal<CreateUpdateCatalogUserControl>(
                   new CreateUpdateCatalogUserControl(new Catalog(), this.SelectedStore),
                   window,
                   "Créer un nouveau magasin");

                if (res)
                    RefreshCatalogs();
            }
        }

        internal void UpdateCatalog(Window window)
        {
            if (this.SelectedCatalog != null && this.SelectedStore != null)
            {
                var res = DialogHelper.ShowCreateOrUpdateModal<CreateUpdateCatalogUserControl>(
                    new CreateUpdateCatalogUserControl(this.SelectedCatalog, this.SelectedStore),
                    window,
                    "Modifier le catalogue");

                if (res == true)
                    this.RefreshCatalogs();
            }
        }

        internal void DeleteCatalog()
        {
            if (AlertHelper.ShowAlert("Voulez-vous vraiment supprimer ce catalogue ? Ceci supprimera tous les produits associés.", AlertHelper.AlertTypes.Question) == MessageBoxResult.Yes)
            {

                if (SelectedCatalog != null)
                {
                    using (var bmContainer = new BusinessModelsContainer())
                    {
                        var selectedCatalog = bmContainer.CatalogSet.First(elem => elem.Id == SelectedCatalog.Id);

                        if (selectedCatalog != null)
                        {
                            bmContainer.CatalogSet.Remove(selectedCatalog);
                            bmContainer.SaveChanges();

                            AlertHelper.ShowAlert("Le catalogue a été supprimé");

                            RefreshCatalogs();
                        }
                    }
                }
            }
        }
        internal void DeletePage()
        {
            if (AlertHelper.ShowAlert("Voulez-vous vraiment supprimer cette page ? Ceci supprimera tous les produits associés.", AlertHelper.AlertTypes.Question) == MessageBoxResult.Yes)
            {

                if (SelectedPage != null)
                {
                    using (var bmContainer = new BusinessModelsContainer())
                    {
                        var selectedPage = bmContainer.PageSet.First(elem => elem.Id == SelectedPage.Id);

                        if (selectedPage != null)
                        {
                            bmContainer.PageSet.Remove(selectedPage);
                            bmContainer.SaveChanges();

                            AlertHelper.ShowAlert("La page a été supprimé");

                            RefreshPages();
                        }
                    }
                }
            }
        }
        internal void AddPage(Window window)
        {
            if (this.SelectedStore != null && this.SelectedCatalog != null)
            {
                var res = DialogHelper.ShowCreateOrUpdateModal<CreateUpdatePageUserControl>(
                   new CreateUpdatePageUserControl(new Page(), this.SelectedCatalog),
                   window,
                   "Créer une nouvelle page");

                if (res)
                    RefreshPages();
            }
            else
            {
                AlertHelper.ShowAlert("Vous devez sélectionner un catalogue !", AlertHelper.AlertTypes.Exclamation);
            }
        }


        internal void AddProduct(Window window)
        {
            if (this.SelectedStore != null && this.SelectedCatalog != null && this.SelectedPage != null)
            {
                var res = DialogHelper.ShowCreateOrUpdateModal<CreateUpdateProductUserControl>(
                   new CreateUpdateProductUserControl(new Product(), this.SelectedPage),
                   window,
                   "Créer un nouveau produit");

                if (res)
                    RefreshProducts();
            }
            else
            {
                AlertHelper.ShowAlert("Vous devez sélectionner une page !", AlertHelper.AlertTypes.Exclamation);
            }
        }

        internal void UpdateProduct(Window window)
        {
            if (this.SelectedProduct != null && this.SelectedPage != null)
            {
                var res = DialogHelper.ShowCreateOrUpdateModal<CreateUpdateProductUserControl>(
                    new CreateUpdateProductUserControl(this.SelectedProduct, this.SelectedPage),
                    window,
                    "Modifier le produit");

                if (res == true)
                    this.RefreshProducts();
            }
        }


        internal void DeleteProduct()
        {
            if (this.SelectedProduct != null)
            {
                if (AlertHelper.ShowAlert("Voulez-vous vraiment supprimer ce produit ?", AlertHelper.AlertTypes.Question) == MessageBoxResult.Yes)
                {
                    using (var bmContainer = new BusinessModelsContainer())
                    {
                        var selectedProduct = bmContainer.ProductSet.First(elem => elem.Id == SelectedProduct.Id);


                        if (selectedProduct != null)
                        {
                            bmContainer.ProductSet.Remove(selectedProduct);
                            bmContainer.SaveChanges();

                            AlertHelper.ShowAlert("Le produit a été supprimé");

                            RefreshProducts();
                        }
                    }
                }
            }
        }
        #endregion

    }
}

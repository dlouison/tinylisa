﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TinyLISA.BackOffice.Interfaces
{
    interface IController<T>
    {
        ObservableCollection<T> GetAll();
        /// <summary>
        /// Insert object toCreate in database.
        /// </summary>
        /// <param name="toCreate">Object to insert in DB.</param>
        /// <returns>The new created object.</returns>
        T Create(T toCreate);
        /// <summary>
        /// Update object toUpdate in database.
        /// </summary>
        /// <param name="toUpdate">Object to update in DB.</param>
        /// <returns>The updated object.</returns>
        T Update(T toUpdate);
        /// <summary>
        /// Delete object toDelete in database.
        /// </summary>
        /// <param name="toDelete">Object to delete in DB</param>
        /// <returns>True if the object has been deleted, false else.</returns>
        bool Delete(T toDelete);
    }
}

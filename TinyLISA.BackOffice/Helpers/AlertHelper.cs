﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TinyLISA.BackOffice.Helpers
{
    static class AlertHelper
    {

        #region Properties

        /// <summary>
        /// Définit un énum contenant les différents types d'état pour les MessageBox.
        /// </summary>
        public enum AlertTypes { Sucess, Info, Exclamation, Question, Warning, Error }

        #endregion


        #region Methods

        /// <summary>
        /// Affiche une MessageBox contenant le message donnée en paramètre, et renvoie le MessageBoxResult en retour.
        /// </summary>
        /// <param name="message">Le message à afficher dans la MessageBox.</param>
        /// <param name="type">Le type de l'alerte à utiliser.</param>
        /// <returns>Le MessageBoxResult qui contient les informations sur le choix de l'utilisateur.</returns>
        public static MessageBoxResult ShowAlert(string message, AlertTypes type = AlertTypes.Sucess)
        {
            switch (type)
            {
                case AlertTypes.Sucess:
                    return MessageBox.Show(message, "Opération réussie.", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                case AlertTypes.Info:
                    return MessageBox.Show(message, "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                case AlertTypes.Exclamation:
                    return MessageBox.Show(message, "Attention !", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                case AlertTypes.Question:
                    return MessageBox.Show(message, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                case AlertTypes.Warning:
                    return MessageBox.Show(message, "Avertissement", MessageBoxButton.OK, MessageBoxImage.Warning);
                case AlertTypes.Error:
                    return MessageBox.Show(message, "Erreur", MessageBoxButton.OK, MessageBoxImage.Error);
                default:
                    return MessageBoxResult.Cancel;
            }
        }

        #endregion


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TinyLISA.BackOffice.Helpers
{
    class DialogHelper
    {

        /// <summary>
        /// Affiche une boite de dialogue qui contient un titre et un controle utilisateur permettant la création ou la mise à jour d'un modèle.
        /// </summary>
        /// <typeparam name="T">Type du controle utilisateur utilisé.</typeparam>
        /// <param name="createOrUpdateUC">Controle utilisateur à afficher.</param>
        /// <param name="parentWindow">Fenêtre parente.</param>
        /// <param name="title">Titre de la boîte de dialogue.</param>
        public static bool ShowCreateOrUpdateModal<T>(T createOrUpdateUC, Window parentWindow, string title = "Créer ou mettre à jour un objet")
        {
            Window modalCreateOrUpdate = new Window()
            {
                Title = title,
                Content = createOrUpdateUC,
                SizeToContent = SizeToContent.WidthAndHeight,
                Owner = parentWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                ResizeMode = ResizeMode.NoResize,
            };
            modalCreateOrUpdate.ShowDialog();

            if (modalCreateOrUpdate.DialogResult != null)
                return modalCreateOrUpdate.DialogResult.Value;
            else
                return false;
        }

    }
}

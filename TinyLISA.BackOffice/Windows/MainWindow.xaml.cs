﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TinyLISA.BackOffice.Helpers;
using TinyLISA.BackOffice.UserControls.Catalogs;
using TinyLISA.BackOffice.UserControls.Stores;
using TinyLISA.BackOffice.ViewModel;
using TinyLISA.BackOffice.Views;
using TinyLISA.Business;

namespace TinyLISA.BackOffice.Windows
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Properties
        public StoreViewModel StoreVM { get; set; }
        public CatalogViewModel CatVM { get; set; }
        #endregion

        #region Constructors


        public MainWindow()
        {
            InitializeComponent();

            // Add the store main user control inside the main window container.
            this.MainWindow_Container.Children.Clear();

            StoreVM = new StoreViewModel();
            CatVM = new CatalogViewModel(StoreVM);

            StoreView storeView = new StoreView() { DataContext = StoreVM };
            CatalogView catalogView = new CatalogView() { DataContext = CatVM };

            storeView.viewmodel.CatalogViewModel = CatVM;

            this.MainWindow_Container.Children.Clear();
            this.MainWindow_Container.Children.Add(storeView);

            storeView.CatalogsContainer.Children.Clear();
            storeView.CatalogsContainer.Children.Add(catalogView);
        }

        #endregion

        #region Methods
        private void Menu_AddStore_Click(object sender, RoutedEventArgs e)
        {
            DialogHelper.ShowCreateOrUpdateModal<CreateUpdateStoreUserControl>(
                new CreateUpdateStoreUserControl(new Store()),
                this,
                "Créer un nouveau magasin");
        }

        private void Menu_AddCatalog_Click(object sender, RoutedEventArgs e)
        {
            if (this.StoreVM != null && this.StoreVM.SelectedStore != null)
            {
                DialogHelper.ShowCreateOrUpdateModal<CreateUpdateCatalogUserControl>(
                    new CreateUpdateCatalogUserControl(new Catalog(), this.StoreVM.SelectedStore),
                    this,
                    "Créer un nouveau catalogue");

                if (this.CatVM != null)
                    this.CatVM.RefreshCatalogs();
            }
            else
            {
                AlertHelper.ShowAlert("Vous devez sélectionner un magasin !", AlertHelper.AlertTypes.Exclamation);
            }
        }

        private void Menu_Quit_Click(object sender, RoutedEventArgs e)
        {
            if (AlertHelper.ShowAlert("Voulez-vous vraiment quitter ?", AlertHelper.AlertTypes.Question) == MessageBoxResult.Yes)
                Window.GetWindow(this).Close();
        }
        #endregion


    }
}

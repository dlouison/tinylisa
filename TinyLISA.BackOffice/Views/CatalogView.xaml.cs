﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TinyLISA.BackOffice.ViewModel;

namespace TinyLISA.BackOffice.Views
{
    /// <summary>
    /// Logique d'interaction pour CatalogView.xaml
    /// </summary>
    public partial class CatalogView : UserControl
    {

        #region Properties

        public CatalogViewModel viewmodel
        {
            get
            {
                return ((CatalogViewModel)this.DataContext);
            }
        }

        #endregion

        #region Constructors

        public CatalogView()
        {
            InitializeComponent();


        }



        #endregion

        #region Methods

        private void ContextMenu_AddCatalog_Click(object sender, RoutedEventArgs e)
        {
            viewmodel.AddCatalog(Window.GetWindow(this));
        }

        private void ContextMenu_UpdateCatalog_Click(object sender, RoutedEventArgs e)
        {
            viewmodel.UpdateCatalog(Window.GetWindow(this));
        }

        private void ContextMenu_DeleteCatalog_Click(object sender, RoutedEventArgs e)
        {
            this.viewmodel.DeleteCatalog();
        }


        private void Hyperlink_AddPage(object sender, RoutedEventArgs e)
        {
            viewmodel.AddPage(Window.GetWindow(this));
        }

        private void ContextMenu_DeleteProduct_Click(object sender, RoutedEventArgs e)
        {
            this.viewmodel.DeleteProduct();
        }

        private void ContextMenu_UpdateProduct_Click(object sender, RoutedEventArgs e)
        {
            this.viewmodel.UpdateProduct(Window.GetWindow(this));
        }

        private void ContextMenu_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            this.viewmodel.AddProduct(Window.GetWindow(this));
        }

        private void Hyperlink_DeletePage(object sender, RoutedEventArgs e)
        {
            this.viewmodel.DeletePage();
        }

        #endregion


    }
}

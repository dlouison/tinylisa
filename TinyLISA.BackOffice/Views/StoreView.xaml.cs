﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TinyLISA.BackOffice.Helpers;
using TinyLISA.BackOffice.UserControls;
using TinyLISA.BackOffice.UserControls.Stores;
using TinyLISA.BackOffice.ViewModel;
using TinyLISA.Business;

namespace TinyLISA.BackOffice.Views
{
    /// <summary>
    /// Logique d'interaction pour StoreListUserControl.xaml
    /// </summary>
    public partial class StoreView
    {

        #region Fields
        /// <summary>
        /// Get the view model instance.
        /// </summary>
        public StoreViewModel viewmodel
        {
            get
            {
                return ((StoreViewModel)this.DataContext);
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Main constructor.
        /// </summary>
        public StoreView()
        {
            InitializeComponent();
            //CatalogViewModel cat = new CatalogViewModel(this.viewmodel);
            //this.CatalogViewModel = cat;

        }
        #endregion

        #region Methods

        /// <summary>
        /// Show a dialog that allows user to create a new Store. 
        /// </summary>
        private void ContextMenu_AddStore_Click(object sender, RoutedEventArgs e)
        {
            viewmodel.AddStore(Window.GetWindow(this));
        }
        /// <summary>
        /// Delete the selected store from the list.
        /// </summary>
        private void ContextMenu_DeleteStore_Click(object sender, RoutedEventArgs e)
        {
            viewmodel.DeleteStore();
        }
        /// <summary>
        /// Show a dialog that allows user to update selected store in list.
        /// </summary>
        /// <param name="sender">The list where the store is selected.</param>
        private void ContextMenu_UpdateStore_Click(object sender, RoutedEventArgs e)
        {
            viewmodel.UpdateStore(Window.GetWindow(this));
        }
        /// <summary>
        /// Shows a dialog to create a new client.
        /// </summary>
        private void ContextMenu_AddClient_Click(object sender, RoutedEventArgs e)
        {
            viewmodel.AddClient(Window.GetWindow(this));
        }
        /// <summary>
        /// Shows a dialog to udpate the selected client.
        /// </summary>
        private void ContextMenu_UpdateClient_Click(object sender, RoutedEventArgs e)
        {
            viewmodel.UpdateClient(Window.GetWindow(this));
        }
        /// <summary>
        /// Shows a dialog to delete client.
        /// </summary>
        private void ContextMenu_DeleteClient_Click(object sender, RoutedEventArgs e)
        {
            viewmodel.DeleteClient();
        }

        #endregion


    }
}

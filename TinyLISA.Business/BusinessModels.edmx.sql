
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/13/2017 19:00:19
-- Generated from EDMX file: C:\Users\Louison Diogo\documents\visual studio 2015\Projects\TinyLISA\TinyLISA.Business\BusinessModels.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [TinyLISA];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_CatalogPage]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PageSet] DROP CONSTRAINT [FK_CatalogPage];
GO
IF OBJECT_ID(N'[dbo].[FK_ClientStore]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ClientSet] DROP CONSTRAINT [FK_ClientStore];
GO
IF OBJECT_ID(N'[dbo].[FK_PageProduct]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ProductSet] DROP CONSTRAINT [FK_PageProduct];
GO
IF OBJECT_ID(N'[dbo].[FK_StoreCatalog]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CatalogSet] DROP CONSTRAINT [FK_StoreCatalog];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[CatalogSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CatalogSet];
GO
IF OBJECT_ID(N'[dbo].[ClientSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClientSet];
GO
IF OBJECT_ID(N'[dbo].[PageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PageSet];
GO
IF OBJECT_ID(N'[dbo].[ProductSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductSet];
GO
IF OBJECT_ID(N'[dbo].[StoreSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StoreSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ProductSet'
CREATE TABLE [dbo].[ProductSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Price] float  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Label] nvarchar(200)  NOT NULL,
    [Page_Id] int  NULL
);
GO

-- Creating table 'PageSet'
CREATE TABLE [dbo].[PageSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Number] int  NOT NULL,
    [Catalog_Id] int  NOT NULL
);
GO

-- Creating table 'CatalogSet'
CREATE TABLE [dbo].[CatalogSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(200)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [ReleaseDate] datetime  NOT NULL,
    [Store_Id] int  NOT NULL
);
GO

-- Creating table 'StoreSet'
CREATE TABLE [dbo].[StoreSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(200)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ClientSet'
CREATE TABLE [dbo].[ClientSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Lastname] nvarchar(100)  NOT NULL,
    [Firstname] nvarchar(100)  NOT NULL,
    [Email] nvarchar(200)  NOT NULL,
    [Phone] nvarchar(15)  NOT NULL,
    [Adress] nvarchar(200)  NOT NULL,
    [PostalCode] nvarchar(8)  NOT NULL,
    [City] nvarchar(200)  NOT NULL,
    [Store_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ProductSet'
ALTER TABLE [dbo].[ProductSet]
ADD CONSTRAINT [PK_ProductSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PageSet'
ALTER TABLE [dbo].[PageSet]
ADD CONSTRAINT [PK_PageSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CatalogSet'
ALTER TABLE [dbo].[CatalogSet]
ADD CONSTRAINT [PK_CatalogSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'StoreSet'
ALTER TABLE [dbo].[StoreSet]
ADD CONSTRAINT [PK_StoreSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClientSet'
ALTER TABLE [dbo].[ClientSet]
ADD CONSTRAINT [PK_ClientSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Page_Id] in table 'ProductSet'
ALTER TABLE [dbo].[ProductSet]
ADD CONSTRAINT [FK_PageProduct]
    FOREIGN KEY ([Page_Id])
    REFERENCES [dbo].[PageSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PageProduct'
CREATE INDEX [IX_FK_PageProduct]
ON [dbo].[ProductSet]
    ([Page_Id]);
GO

-- Creating foreign key on [Catalog_Id] in table 'PageSet'
ALTER TABLE [dbo].[PageSet]
ADD CONSTRAINT [FK_CatalogPage]
    FOREIGN KEY ([Catalog_Id])
    REFERENCES [dbo].[CatalogSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CatalogPage'
CREATE INDEX [IX_FK_CatalogPage]
ON [dbo].[PageSet]
    ([Catalog_Id]);
GO

-- Creating foreign key on [Store_Id] in table 'CatalogSet'
ALTER TABLE [dbo].[CatalogSet]
ADD CONSTRAINT [FK_StoreCatalog]
    FOREIGN KEY ([Store_Id])
    REFERENCES [dbo].[StoreSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StoreCatalog'
CREATE INDEX [IX_FK_StoreCatalog]
ON [dbo].[CatalogSet]
    ([Store_Id]);
GO

-- Creating foreign key on [Store_Id] in table 'ClientSet'
ALTER TABLE [dbo].[ClientSet]
ADD CONSTRAINT [FK_ClientStore]
    FOREIGN KEY ([Store_Id])
    REFERENCES [dbo].[StoreSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ClientStore'
CREATE INDEX [IX_FK_ClientStore]
ON [dbo].[ClientSet]
    ([Store_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------